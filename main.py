import sqlite3
import sys
from datetime import datetime


def main():
    def create_db():
        db_connection = sqlite3.connect("test_db.db")
        c = db_connection.cursor()
        c.execute("""CREATE TABLE transport_types 
                    (
                    id integer,
                    name text
                    )"""
                )
        c.execute("""CREATE TABLE transport
                    (
                    id integer,
                    number text,
                    type text,
                    creation_date text,
                    transport_type_id integer,
                    FOREIGN KEY(transport_type_id) REFERENCES transport_types(id)
                    )"""
                  )
        c.execute("""CREATE TABLE users
                    (
                    id integer,
                    name text,
                    surname text,
                    work_from text,
                    is_active integer
                    )"""
                  )
        c.execute("""CREATE TABLE links_users_works
                    (
                    user_id integer,
                    transport_id integer, 
                    date_of_receiving text,
                    date_of_ending text,
                    money_received integer,
                    money_spend integer,
                    FOREIGN KEY(user_id) REFERENCES users(id),
                    FOREIGN KEY(transport_id) REFERENCES transport(id)
                    )"""
                  )
        db_connection.commit()
        db_connection.close()

    def add_user(id, name, surname, work_from, is_active):
        db_connection = sqlite3.connect("test_db.db")
        c = db_connection.cursor()
        c.execute("INSERT INTO users VALUES (:id, :name, :surname, :work_from, :is_active)",
                    (id, name, surname, work_from, is_active)
                )
        db_connection.commit()
        db_connection.close()

    def add_transport(id, number, type, creation_date, transport_id):
        db_connection = sqlite3.connect("test_db.db")
        c = db_connection.cursor()
        c.execute("INSERT INTO transport VALUES (:id, :number, :type, :creation_date, :transport_type_id)",
                  (id, number, type, creation_date, transport_id)
                  )
        db_connection.commit()
        db_connection.close()

    def add_transport_type(id, name):
        db_connection = sqlite3.connect("test_db.db")
        c = db_connection.cursor()
        c.execute("INSERT INTO transport_types VALUES (:id, :name)",
                  (id, name)
                  )
        db_connection.commit()
        db_connection.close()

        return "test"

    def calculate(user_id, data):
        return "smth"

    create_db()
    add_transport_type(123, "moto")
    add_transport_type(321, "auto")
    add_transport(1, "wAb0sa", "volvo", "1997-10-10", 321)
    add_user(1, "Ivan", "Ivanov", "2020-10-10", 1)


if __name__ == '__main__':
    main() # -a, -at, -aty, -c
